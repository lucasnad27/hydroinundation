## Stormwater Inundation Modeling Suite

This set of python scripts is designed to assist engineers in determining how flood events will effect the landscape. This toolset is in its infancy and only basic functionality has been built. A list of initial dependencies are listed below:

- Python 2.7
- [GDAL](http://www.gdal.org)
- [GDAL python bindings](https://pypi.python.org/pypi/GDAL/)
- [numpy](https://pypi.python.org/pypi/numpy)