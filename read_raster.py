import gdal
from gdalconst import GA_ReadOnly
# import struct

filename = 'data/1mX1m-ljc.tif'
dataset = gdal.Open(filename, GA_ReadOnly)

if dataset is None:
    print 'not yet'

band = dataset.GetRasterBand(1)
#Loop through every row starting at the bottomleft.
print band.YSize
for i in range(band.YSize - 1, -1, -1):
    scanline = band.ReadAsArray(0, i, band.XSize, 1, band.XSize, 1)
