import unittest
from hydro.hydroreader import (
    HydroReader,
    HydroValidityError)


class SimplisticTest(unittest.TestCase):

    def setUp(self):
        self.hr = HydroReader(
            data_dir='/Users/lucasnad27/Dev/hydromodel/data',
            output_dir='/Users/lucasnad27/Dev/hydromodel/output')

    def test_valid_dir_input(self):
        hr = HydroReader(
            data_dir='/Users/lucasnad27/Dev/hydromodel/data',
            output_dir='/Users/lucasnad27/Dev/hydromodel/output')
        self.assertTrue(hr)

    def test_source_coords(self):
        self.assertIsNotNone(self.hr.get_source_coords())

if __name__ == '__main__':
    unittest.main()
