#only use for general testing and hacking
#it is crap for good structure and PEP-8

import gdal
from gdalconst import *
import numpy as np
import logging

# filename = 'data/1mX1m-ljc.tif'
filename = 'data/stormdrains.tif'
dataset = gdal.Open(filename, GA_ReadOnly)

logging.basicConfig(
    filename='rasteroutput.log',
    level=logging.DEBUG,
    filemode='w'
)

if dataset is None:
    logging.critical('No raster data found at %s location' % filename)

band = dataset.GetRasterBand(1)
md_array = band.ReadAsArray()
#find all indices where there is a storm drain
elements = np.where(md_array == 1)
#unpack the x,y coordinates
(x, y) = elements
#TODO: Make this more pythonic! Not very readable right now
for i in range(len(elements[0]) - 1):
    logging.debug('X: %s; Y: %s' % (x[i], y[i]))
exit()

#Loop through every row starting at the bottomleft.
print band.YSize
raster_lines = []
for i in range(band.YSize - 1, -1, -1):
    scanline = band.ReadAsArray(0, i, band.XSize, 1, band.XSize, 1)
    raster_lines = raster_lines + scanline
    # logging.debug(scanline.shape)
    # exit()
    # print scanline
multid_array = np.array(raster_lines)
print multid_array.shape
