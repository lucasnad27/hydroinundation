import gdal
from gdalconst import GA_ReadOnly
import os
import numpy as np


class HydroReader():
    '''
        HydroReader is intended to be a utility class to help
        aid the process of reading/manipulating raster data that represents
        either a digital elevation model (DEM) or a boolean raster representing
        where stormwater drains are located.
        Keyword arguments:
            data_dir -- directory for all input data
            output_dir -- directory for all output data
            ptsource_raster -- filename of drain boolean raster (default 'source.tif')
            dem -- filename of DEM to be used in analysis (default 'elevation.tif')
    '''
    def __init__(self, data_dir, output_dir, ptsource_file='source.tif', dem_file='elevation.tif'):
        self.output_dir = output_dir
        self.ptsource_file = os.path.join(data_dir, ptsource_file)
        self.dem_file = os.path.join(data_dir, dem_file)
        self._readrasters()

    def get_source_coords(self):
        '''
            Unpacks a tuple containing two arrays: One containing
            x-values, the other containing y-values
        '''
        elements = np.where(self.ptsource == 1)
        x, y = elements
        return x, y

    def _readrasters(self):
        '''Reads in raster files as numpy arrays and checks for validity
        '''
        self.dem = self._create_array(self.dem_file)
        self.ptsource = self._create_array(self.ptsource_file)
        self._compare_rasters(self.dem, self.ptsource)

    def _create_array(self, infile):
        '''Checks the validity of a hydro raster dataset
        '''
        dataset = gdal.Open(infile, GA_ReadOnly)
        if dataset is None:
            raise HydroValidityError(
                'Cannot find raster data. Please verify existense of %s'
                % infile)
        band = dataset.GetRasterBand(1)
        array = band.ReadAsArray()
        return array

    def _compare_rasters(self, array_1, array_2):
        '''Insures the input rasters are of same dimension
            TODO: Create more stringent comparison methods
        '''
        if array_1.shape != array_2.shape:
            raise HydroValidityError('Input rasters are not the same shape')


class HydroValidityError(Exception):
    def __init__(self, message):
        self.message = message
